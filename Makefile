
SRC_DIR = ./src
SRC = $(SRC_DIR)/World.cpp
SRC += $(SRC_DIR)/Guess.cpp
SRC += $(SRC_DIR)/GameSettings.cpp
SRC += $(SRC_DIR)/GameEngine.cpp

LIB_DIR = ./lib

LIB = -L$(LIB_DIR) -I$(LIB_DIR) -I$(SRC_DIR)
all:
	g++-7 $(SRC) $(LIB) -std=c++1z -Wall -Wextra -g -O3 -o ./bin/world
