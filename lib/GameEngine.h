#pragma once

/**
 * @file GameEngine.h
 * @author Jens Schønberg
 * @date 04-07-2017
 * @version 2.0.0
 *
 * @brief Game is responsible for running the main game loop and building the other components.
 *
 * @section DESCRIPTION
 * Gameengine acts as a composite for the other subcomponents, deligating the tasks to each sub-component through its main loop.
 */

#include "GameSettings.h"
#include "Guess.h"

#include <string>
#include <ctime>
#include <random>
#include <unordered_set>
#include <climits>
#include <ios>

/*
 * @brief The GameEngine handles the control flow of the game.
 *
 * @section DESCRIPTION
 * The Game Engine also configures the run-time settings. Such as number of rounds, if it should be duplicate mode or not. And in the case of more modes, it'll designate which modes have been selected.
 */
class GameEngine{
	uint8_t m_rounds; // the amount of guesses for a given game
	std::vector<Guess> m_history; // the guess history
	std::string m_playername;
	bool m_duplicate; // if duplicate is allowed

	Guess m_secret; // the secret, which the player is trying to guess.
	
	// some const static text fields for writing out

	const std::string m_welcome = R"###(
		Welcome to Mastermind, now with more KISS!
		The game needs to be setup before running.
		Please configure the game settings before playing!
		Press 'c' to continue or 'q' to quit.
			)###";

	const std::string m_setup0 = R"###(
		Please enter the ammound ot guesses per game round: 
			)###";
	const std::string m_setup1 = R"###(
		Please enter if duplicated entries are allowed [Y/y] or [N/n]
			)###";
	const std::string m_win = R"###(
		Congratulation, you have won!
			)###";
	const std::string m_next = R"###(
		Want to try again? [Y/y] or [N/n]
			)###";
	public:

	
	GameEngine();
	
	// prints welcomescreen
	void welcome();

	// sets up the rounds and writes a setup manual
	// it also queries the user for setting duplication
	// to on/off
	void setup();
	
	// retreives a guess from stdin, deprecated
	Guess getGuess();

	// generate a random secret pattern
	void generateSecret();

	// main loop for running the game.
	void run();

	// writes out the history in the end, if the player
	// didn't win.
	void writeHistory();

	// generates and converts {0,1,2} integers
	// into {_,O,!}.
	// ! indicates a right type and right position
	// O indicates a right type, but wrong position
	// _ is a wrong type and wrong position
	bool generateOutput(std::vector<int> response);

	// display the winning text
	void displayWin();

	// deprecated, used to ask if the player wanted
	// to play another round.
	bool displayNext();
};
