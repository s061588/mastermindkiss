#pragma once

/**
 * @file GameSettings.h
 * @author Jens Schønberg
 * @date 04-07-2017
 * @version 2.0.0
 *
 * @brief GameSettings services and provisions the game settings. Such as the rules.
 *
 * @section DESCRIPTION
 * The settings class provisions the "static" rules, while the game engine contains the rules which are dynamic to a session, such as number of guesses, etc.
 * The GameSettings is a singleton, being able to pull down an intance from any nesting of the application code and ensuring that there only one active sessions, as to avoid mismatches of rules.
 * The Singleton implementation is not thread-safe!
 */

#include <memory>


/**
 * @brief POD for handling the configuration of the game session.
 */
struct GameMode{
	std::string m_name;
	uint8_t m_holes;
	uint8_t m_color;
};

/**
 * @brief GameSettings are the main provider for game session configurations.
 *
 * @section DESCRIPTION
 * Because GameSetting is the main provisioner of GameModes, this allows to add and remove any new gamesmodes if needed or if the program requires to dynamically change or modify them.
 */
class GameSettings {
	
	//instance
	static GameSettings* m_instance;

	//Current running instance of the game mode.
	GameMode m_current;

	// For the Singleton
	GameSettings();
	public:
	// Get/Create instance
	static GameSettings* getInstance();

	// Supply a new game mode
	void supplyMode(GameMode & m);

	// Getter
	GameMode& getCurrent();
};
