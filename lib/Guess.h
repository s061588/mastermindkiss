#pragma once
/* @file Guess.h
 * @author Jens Schønberg
 * @date 04-07-2017
 * @version 2.0.0
 *
 * @brief header file for Guess, wrapper class for colors and holes.
 *
 * @section DESCRIPTION
 * Ensures that guesses are provides console input/output support.
 * Ensures that the guesses hare comparable and returns a response vector.
 *
 * The Guess contains,
 * 	m_guess : std::vector<int>
 * 	m_ucontent : std::unordered_set<int>
 * 	m_dcontent : std::unordered_map<int,int>
 *
 * The m_guess contains the raw int for the guess.
 * The set is used to match against uniquely defined pegs.
 * The map is used to match against possible dublicated pegs.
 */

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <iostream> // for istream and ostream
#include <algorithm> // for any_of and unique_copy
#include <utility>
#include <cassert>

class Guess{
	std::vector<int> m_guess; //internal representation of guess.
	bool m_duplicate;

	std::unordered_set<int> m_ucontent; // used to compare unique content
	std::unordered_map<int,int> m_dcontent; // used to compare duplicate content
	
	// used to populate m_dcontent : m_ucontent, depending on
	// allowance of duplication
	void populateDuplicate();
	void populateUnique();

	public:
	Guess(bool duplicate=false);

	// used for debugging.
	Guess(std::vector<int> guess, bool duplicate); // Provide a ready-guess, wrap it.
	
	void assign(std::vector<int> & guess, bool duplicate);

	/**
	 * @brief Compares two guess, and implements a algorithm for supplying a vector of right guess (key-peg)
	 *
	 * @param other, the Guess object to compare to.
	 * @return response, a vector which designates;
	 * 	2 : match type and position
	 * 	1 : match type, but not position
	 * 	0 : match neither typie or position
	 */

	std::vector<int> operator==(Guess & other); // used for comparing and returning key-regs.
	
	/**
	 * @brief Support stdout and stdin streams for reading and writing out
	 *
	 * @param stream, either an istream or ostream
	 * @param data, the data to be transmitted to/from stream
	 * @return stream, returns the modified stream
	 */

	friend std::ostream &operator<<(std::ostream &output, const Guess & data_out); // Used for writing out a guess.
	friend std::istream &operator>>(std::istream &input, Guess & data_in); // Used for loading in a guess.
};
