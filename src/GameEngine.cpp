#include "GameEngine.h"

GameEngine::GameEngine(){
	m_rounds = 0;
};

void GameEngine::welcome(){
	std::cout << m_welcome;
	char cmd;
	bool running = true;
	while(running){
		std::cin >> cmd;
		switch(cmd){
			case 'c': running = false; break;
			case 'q': running = false; break;
			default: continue;
		}
	}
}

void GameEngine::setup(){
	std::cout << m_setup0;
	std::cin >> m_rounds;
	std::cout << m_setup1;
	char a;
	std::cin >> a;
	switch(a){
		case 'y' :
		case 'Y' : m_duplicate = true; break;
		case 'n' :
		case 'N' : 
		default: m_duplicate = false;
	}
}

Guess GameEngine::getGuess(){
	Guess g;
	std::cin >> g;
	return g;
}

void GameEngine::generateSecret(){
	auto mode = GameSettings::getInstance()->getCurrent();

	// basic time based seeding.
	std::srand( std::time ( 0 ));

	std::vector<int> secret;
	std::unordered_set<int> no_dup;
	// populate the secret key
	if(m_duplicate){
		for(int i = 0; i < mode.m_holes; ++i){
			secret.emplace_back(rand() % mode.m_color + 1);
		}
	} else {
		while(no_dup.size() < mode.m_holes){
			no_dup.emplace(rand() % mode.m_color + 1);
		}
		for(auto i : no_dup) secret.push_back(i);
	}
	m_secret.assign(secret,m_duplicate);
}

bool GameEngine::generateOutput(std::vector<int> response){
	bool win = true;
	for(auto & i : response){
		switch(i){
			case 0: win &= false; std::cout << "_"; break;
			case 1: win &= false; std::cout << "O"; break;
			case 2: win &= true; std::cout << "!"; break;
			default : std::cout << "_";
		}
		std::cout << ' ';
	}
	std::cout << '\n';
	return win;
}

void GameEngine::displayWin(){
	std::cout << m_win;
}


bool GameEngine::displayNext(){
	std::cout << m_next;
	char a;
	std::cin >> a;
	switch(a){
		case 'n':
		case 'N': return false;
		case 'y':
		case 'Y': 
		default : return true;
	}
}

void GameEngine::writeHistory(){
	for(auto & i : m_history) {
		std::cout << i;
	}
}

void GameEngine::run(){
	auto mode = GameSettings::getInstance()->getCurrent();
	welcome();
	setup();
	std::cout << "Generating secret key ... " << '\n';
	generateSecret();
	std::cout << "ready to play, enter 4 values between 1 and "
		<< (int)mode.m_color;
	bool running = true;
	uint8_t rounds = 0;
	while(running && !(rounds & m_rounds)){
		std::cout << '\n' <<  '\n'; // flushes the std::cin
		Guess g(m_duplicate); // initialize a guess
		std::cin >> g; // populate a guess
		m_history.emplace_back(g); // save to history
		auto response = g == m_secret; // makes the compare with the secet pattern.
		auto win = generateOutput(response); // returns the winning boolean
		if(win) {
			displayWin();
			return;
		}
		++rounds;
		std::cin.clear();
		std::cin.ignore();
	}
	std::cout << "better luck next time!" << std::endl;
	std::cout << "you guessed at :" << '\n';
	writeHistory();
	delete GameSettings::getInstance();
}

