#include "GameSettings.h"

GameSettings* GameSettings::m_instance = nullptr;

GameSettings* GameSettings::getInstance(){
	if(!m_instance)
		m_instance = new GameSettings();
	return m_instance;
}


// Default behavior is the Original
GameSettings::GameSettings(){
	GameMode mode;
	mode.m_name = "Original";
	mode.m_holes = 4;
	mode.m_color = 6;
	m_current = mode;
}

// Allow to change it though
void GameSettings::supplyMode(GameMode & m){
	m_current = m;
}

GameMode& GameSettings::getCurrent(){
	return m_current;
}
