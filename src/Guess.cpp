#include "Guess.h"
#include "GameSettings.h"

Guess::Guess(bool duplicate){
	auto mode = GameSettings::getInstance()->getCurrent();
	m_guess = std::vector<int>(mode.m_holes,0);
	m_duplicate = duplicate;
}

Guess::Guess(std::vector<int> guess, bool duplicate){
	m_guess = guess;
	m_duplicate = duplicate;
	if(duplicate) populateDuplicate();
	else populateUnique();
}

void Guess::assign(std::vector<int>& guess, bool duplicate)
{
	m_guess = guess;
	m_duplicate= duplicate;
	if(duplicate) populateDuplicate();
	else populateUnique();
}

std::vector<int> Guess::operator==(Guess & other){
	auto mode = GameSettings::getInstance()->getCurrent();
	
	// response
	std::vector<int> response(mode.m_holes,0);

	// so in here we want to check if duplicates are allowed
	// to branch the verification / comparison.
	
	if(m_duplicate){
		// Allowing duplicates.
		// This means that we only want to compare an even match
		// of types.
		// Correct position and type takes percedence.
		// Use tmp, to not alter the original map.
		std::vector<int> tmp0 = this->m_guess;
		std::unordered_map<int,int> tmp = other.m_dcontent;
		for(int i = 0; i < mode.m_holes; ++i){
			if(tmp0[i] == other.m_guess[i]){
				response[i] = 2;
				tmp0[i] = 0;
				tmp[other.m_guess[i]]--;
			}
		}
		for(int i = 0; i < mode.m_holes; ++i){
			if(tmp[tmp0[i]] > 0){
				response[i] = 1;
				tmp[tmp0[i]]--;
			}
		}

	} else {
		for(int i = 0; i < mode.m_holes; ++i){
			//int choice = 0;
			if(this->m_guess[i] == other.m_guess[i]) response[i] = 2;
			else if(other.m_ucontent.find(this->m_guess[i])
						!= other.m_ucontent.end()) response[i] = 1;
			//else choice = 0;
			//response.emplace_back(choice);
			//response[i] == choice;
		}
	}
	return response;
}

void Guess::populateDuplicate(){
	auto mode = GameSettings::getInstance()->getCurrent();

	for(int i = 0; i < mode.m_color; ++i) m_dcontent.emplace(std::make_pair(i,0));
	for( auto & i : m_guess) m_dcontent[i]++;
}

void Guess::populateUnique(){
	for(auto & i : m_guess) m_ucontent.emplace(i);
}

// recall that friend ostream on global scope
// doesn't need a Guess:: preffix, since this* is compiler
// implicit

std::ostream& operator<<(std::ostream &output, const Guess & data_out){
	for(auto & i : data_out.m_guess) std::cout << i << " ";
	std::cout << '\n';
}

std::istream& operator>>(std::istream &input, Guess & data_in){
	//grab settings
	auto mode = GameSettings::getInstance()->getCurrent();

	//Load in integers into guess.
	if(data_in.m_guess.empty()){
		int tmp = 0;
		for(int i = 0; i < mode.m_holes; ++i) {
			std::cin >> tmp;
			data_in.m_guess.push_back(tmp);
		}
	} else if(data_in.m_guess.size() == mode.m_holes) {
		for(int i = 0; i < mode.m_holes; ++i) std::cin >> data_in.m_guess[i];
	}
	
	//Verify content is within regions of type (between 1 and m_color).
	//Verify that the content doesn't contain duplicates if specified
	

	if(std::any_of(data_in.m_guess.begin(), data_in.m_guess.end(),
				[&](int i){
					return i < 0 || i > mode.m_color;
				})){
		std::cout << "The guess is invalid, please specify valid type "
			<< "between 1 and "
			<< mode.m_color
			<< '\n';
		data_in.m_guess = std::vector<int>(mode.m_holes,0);
	}

	//unique_copy points to the end of a copied container.
	//only unique elements will be copied.
	//Then if the iterator doesn't match the end of the original.
	//This would mean that the original contained duplicates.
	if(!data_in.m_duplicate){
		auto it = std::unique(data_in.m_guess.begin(),
				data_in.m_guess.end());
		if(it != data_in.m_guess.end()){
			std::cout << "The guess is invalid, rules specify no duplicates \n";
			data_in.m_guess = std::vector<int>(mode.m_holes,0);
		}
	}

	std::vector<int> faulty(mode.m_holes,0);
	if(data_in.m_guess != faulty){
		(data_in.m_duplicate ? data_in.populateDuplicate() : data_in.populateUnique());
	}
}
