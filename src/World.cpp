/**
 * @file World.cpp
 * @author Jens Schønberg
 * @date 04-07-2017
 * @version 2.0.0
 *
 * @brief Main application the mastermind game.
 *
 * @section DESCRIPTION
 * World is used as the main file container.
 */


#include "GameSettings.h"
#include "Guess.h"
#include "GameEngine.h"
#include <iostream>
#include <memory>

int main(int argc, char* argv[]){
	auto game = std::make_unique<GameEngine>();
	game->run();
}
